* #### 前言
* #### 基本原理
* #### 数据结构
    * ##### 双向链表（LinkedList所采用的数据结构）
 * #### 常用方法源码剖析
     * ##### 插入元素
          * ###### add()，默认就是在队列的尾部插入一个元素，在那个双向链表的尾部插入一个元素
          * ###### addLast()  与add()方法是一样的，也是在尾部插入一个元素
          * ###### add(index, element)，是在队列的中间插入一个元素
          * ###### addFirst()，在队列的头部插入一个元素
      * ##### 换取元素
          * ###### getFirst() == peek()：获取头部的元素
          * ###### getLast(): 获取尾部的元素
          * ###### get(): 获取元素
     * ##### 删除元素
         * ###### removeLast()：删除最后一个
         * ###### removeFirst() == poll()：删除第一个
         * ###### remove(int index)：删除指定的index

  * #### 总结
      * ##### 优点
      * ##### 缺点
     
* * *

> ### `前言`

##### &emsp; &emsp; LinkedList 是我们日常开发中使用比较常见的一个集合，今儿我们通过源码来看看它底层是怎么来实现的，了解了解它的优缺点和真正适合的场景。

* * *
> ### `基本原理`

##### &emsp; &emsp; LinkedList的底层是基于`双向链表`来实现的。

* * *
> ### `数据结构`

  *   ####  `双向链表`
     
```
private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }
```
    
   ####  图解（假设这个链表中有三个元素[Node]）：
   ![99ca9d636f3fa6ee3f71c529fd4acedf](02-java集合之LinkedList源码剖析.resources/59-LinkedList 双向链表.png)
   
* * *


>#### `常用方法源码剖析`

> ##### `插入元素`
   * ##### ` add()`，默认就是在队列的尾部插入一个元素，在双向链表的尾部插入一个元素
   * ##### ` addLast()  与add()方法是一样的`，也是在双向链表的尾部插入一个元素
    
```
    /**
     * Appends the specified element to the end of this list.
     *
     * <p>This method is equivalent to {@link #addLast}.
     *
     * @param e element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     * add()，默认就是在队列的尾部插入一个元素，在那个双向链表的尾部插入一个元素
     */
    public boolean add(E e) {
        linkLast(e);
        return true;
    }
    
    /**
     * Appends the specified element to the end of this list.
     *
     * <p>This method is equivalent to {@link #add}.
     *
     * @param e the element to add
     * addLast()  与add()方法是一样的，也是在双向链表的尾部插入一个元素
     */
    public void addLast(E e) {
        linkLast(e);
    }
    
    
    /**
     * Links e as last element.
     * 新加入的元素作为最后一个
     */
    void linkLast(E e) {
        // 指针 l 指向 last
        final Node<E> l = last;
        
        // 新元素 e -> new 一个 Node => newNode
        final Node<E> newNode = new Node<>(l, e, null);
        
        // last指针指向newNode
        last = newNode;
        
        //判断l是否为null
        if (l == null)
            // l为null，fist指向newNode
            first = newNode;
        else
            // l 不为null， l的下一个元素指向 newNode
            l.next = newNode;
            
        // linkedList大小+1
        size++;
        modCount++;
    }
```
> ####  图解（假设这个链表中有三个元素[Node]）：
![3ad55795325533e6b21f8b431ff1701e](02-java集合之LinkedList源码剖析.resources/59-LinkedList2.png)


***

   * #####  `add(index, element) `，是在队列的中间插入一个元素
   
```
    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param index index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public void add(int index, E element) {
        // 判断当前传入的index是否越界
        checkPositionIndex(index);
        
        if (index == size)
            // 如果传入的index == 当前链表的大小
            // 就往双向链表的尾部插入一个元素
            linkLast(element);
        else
            // 如果传入的index ！= 当前链表的大小
            // 指定链表的位置的元素前边添加传入的元素
            // node(index)
            // linkBefore(element, node(index));
            linkBefore(element, node(index));
    }
    
    // 判断当前传入的index是否越界
     private void checkPositionIndex(int index) {
        if (!isPositionIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }
    
    /**
     * Returns the (non-null) Node at the specified element index.
     * 经典链表遍历的代码
     */
    Node<E> node(int index) {
        // assert isElementIndex(index);

        // size >> 1 = size / 2
        
        // 判断index 是在链表的前半部分还是后半部分
        if (index < (size >> 1)) {
            // index在链表的前半部分
            Node<E> x = first;
            // 从队列的对头开始遍历
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            // index在链表的后半部分
            Node<E> x = last;
            // 从队列的队尾遍历
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }
    
     /**
     * Inserts element e before non-null Node succ.
     */
    void linkBefore(E e, Node<E> succ) {
        // assert succ != null;
        final Node<E> pred = succ.prev;
        final Node<E> newNode = new Node<>(pred, e, succ);
        succ.prev = newNode;
        if (pred == null)
            first = newNode;
        else
            pred.next = newNode;
        size++;
        modCount++;
    }
    
```

> ####  `Node<E> node(int index)` 图解：
![ad74acc630c3125d67faff533c9d81f8](02-java集合之LinkedList源码剖析.resources/59-LinkedList add(e, index) (3).png)


#### size >> 1，比如说现在是size = 4，size >> 1相当于是size / 2，整除，得到的是2
#### index < (size >> 1)，如果index < (size / 2)，
#### 已知 index = 1，要插入的位置，是在队列的前半部分，那么就会从队列头部开始遍历，找到index那个位置的Node, 也就是第二个Node元素

> #### 接下来就是 `void linkBefore(E e, Node<E> succ)` :
![46a503a9fe1c0731a464b97ad8381b7b](02-java集合之LinkedList源码剖析.resources/59-LinkedList add(e, index)2.png)

* * *
> ##### `换取元素`
   * ##### ` getFirst() == peek() `：获取头部的元素
   * ##### `getLast()`:  获取尾部的元素
   * ##### `get()`: 获取元素
```
 /**
     * Returns the first element in this list.
     *
     * @return the first element in this list
     * @throws NoSuchElementException if this list is empty
     */
    public E getFirst() {
        final Node<E> f = first;
        if (f == null)
            throw new NoSuchElementException();
        return f.item;
    }
    
    /**
     * Retrieves, but does not remove, the head (first element) of this list.
     *
     * @return the head of this list, or {@code null} if this list is empty
     * @since 1.5
     */
    public E peek() {
        final Node<E> f = first;
        return (f == null) ? null : f.item;
    }
    
    /**
     * Returns the last element in this list.
     *
     * @return the last element in this list
     * @throws NoSuchElementException if this list is empty
     */
    public E getLast() {
        final Node<E> l = last;
        if (l == null)
            throw new NoSuchElementException();
        return l.item;
    }
    
    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }
    
    // 上述文字已经写过如何遍历链表或者元素了
     node(index).item;
```

***

>  ##### `删除元素`
   * ##### `removeLast()`：删除最后一个
   * ##### `removeFirst() == poll()`：删除第一个
   * ##### `remove(int index)`：删除指定的index
```
    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from this list
     * @throws NoSuchElementException if this list is empty
     */
    public E removeLast() {
        final Node<E> l = last;
        if (l == null)
            throw new NoSuchElementException();
        return unlinkLast(l);
    }
    
    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element from this list
     * @throws NoSuchElementException if this list is empty
     */
    public E removeFirst() {
        final Node<E> f = first;
        if (f == null)
            throw new NoSuchElementException();
        return unlinkFirst(f);
    }
    
    /**
     * Retrieves and removes the head (first element) of this list.
     *
     * @return the head of this list, or {@code null} if this list is empty
     * @since 1.5
     */
    public E poll() {
        final Node<E> f = first;
        return (f == null) ? null : unlinkFirst(f);
    }
 
    /**
     * Retrieves and removes the head (first element) of this list.
     *
     * @return the head of this list
     * @throws NoSuchElementException if this list is empty
     * @since 1.5
     */
    public E remove() {
        return removeFirst();
    }

     /**
     * LInkedList 删除第一个元素
     * Unlinks non-null first node f.
     */
    private E unlinkFirst(Node<E> f) {
        // assert f == first && f != null;
        // 要删除的Node
        final E element = f.item;
        
        //获取Node的下一个元素
        final Node<E> next = f.next;
        
        // 当前Node设置为null
        f.item = null;
        
        // 当前Node下一个指向为Null
        f.next = null; // help GC
        
        // first指针指向next
        first = next;
        
        //判断next指向的元素是否为null
        if (next == null)
            // 最后一个节点为null
            last = null;
        else
            // next的上一个节点为null，此时next为链表第一个节点
            next.prev = null;
            
        //链表大小-1
        size--;
        modCount++;
        return element;
    }
    
    /**
     * LInkedList 删除最后一个元素
     * Unlinks non-null last node l.
     */
    private E unlinkLast(Node<E> l) {
        // assert l == last && l != null;
        final E element = l.item;
        final Node<E> prev = l.prev;
        l.item = null;
        l.prev = null; // help GC
        last = prev;
        if (prev == null)
            first = null;
        else
            prev.next = null;
        size--;
        modCount++;
        return element;
    }
```

> ####  ` unlinkFirst(Node<E> f)`  （这里以LinkedList 删除第一个元素为例）图解：
![118958a4bebe8bc573df02b9bad18b78](02-java集合之LinkedList源码剖析.resources/59-LinkedList unlinkFirst.png)

***

> ### `总结`

   * #### ` 优点`
        * #### 基于于链表来实现的，非常适合各种元素频繁的插入链表里面去 ,所以不会出现任何的大量元素的挪动，也不会出现说数组的扩容，适合做队列；（队中插入数据的时候没有队头和队尾性能那么好，因为要走一个遍历到指定位置）
       
   
   * #### `缺点 `
       * #### 如果是要随机位置获取一个元素，gett(int index)这个方法，会导致需要遍历，如果里面的数据很多的话，遍历的性能还是比较差的，才能获取到一个随机位置的元素.
   